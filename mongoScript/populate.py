import json
import os
from pymongo import MongoClient
from bson import ObjectId
from dateutil import parser

MONGO_URI = os.getenv('MONGODB_URI', "mongodb://localhost:27017/")

try:
    mongo_client = MongoClient(MONGO_URI)
    db = mongo_client["contasimples"]

    db["empresas"].drop()
    db["transacoes"].drop()

    empresas_collection = db["empresas"]
    transacoes_collection = db["transacoes"]

    with open('empresas.json', encoding='utf-8') as f:
        empresas_data = json.load(f)

    with open('transacoes.json', encoding='utf-8') as t:
        transacoes_data = json.load(t)

    for empresa in empresas_data:
        oid = ObjectId(empresa["_id"])
        empresa["_id"] = oid

    for transacao in transacoes_data:
        oid = ObjectId(transacao["empresaId"])
        transacao["empresaId"] = oid

        timestamp = parser.parse(transacao["dataTransacao"])
        transacao["dataTransacao"] = timestamp


    if isinstance(empresas_data, list):
        empresas_collection.insert_many(empresas_data)
    else:
        empresas_collection.insert_one(empresas_data)


    if isinstance(transacoes_data, list):
        transacoes_collection.insert_many(transacoes_data)
    else:
        transacoes_collection.insert_one(transacoes_data)

    print("Inserção realizada com sucesso!")
    mongo_client.close()
except Exception as e:
    print("ERRO: ", e)