const Empresas = require('./../models/empresas');

let auth = (req, res, next) => {
    let token = req.cookies.auth;
    Empresas.findByToken(token, (err, empresa) => {
        if (err) { throw err };
        if (!empresa) return res.status(400).json({ isAuth: false, msg: "Este usuário/empresa não está logada" });
        req.token = token;
        req.empresa = empresa;
        next();
    })
}

module.exports = { auth };