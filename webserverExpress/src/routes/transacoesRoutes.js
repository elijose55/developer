const express = require("express");
const _ = require("underscore");
const transacoesRouter = express.Router();
const Transacoes = require("../models/transacoes");
const {auth} = require("../middlewares/auth");


// Listar as transacoes de uma empresa especificada pelo ID
transacoesRouter.get('/:empresaId', async (req, res, next) => {
    try {
        let empresaId = req.params.empresaId;
        const result = await Transacoes.find({ 'empresaId': empresaId });
        if (result.length == 0) {
            return res.status(404).json({ error: `Transacoes com empresaId '${empresaId}' não encontradas ou vazias` });
        }
        return res.status(200).send(result);
    } catch (err) {
        console.log(err);
        next(err);
    }
});


// Listar a ultima transacao da empresa logada
transacoesRouter.get('/empresa/ultima', auth, async (req, res, next) => {
    try {
        const empresa = req.empresa;

        const empresaId = empresa._id;
        const result = await Transacoes.find({ 'empresaId': empresaId }).sort({'dataTransacao': -1}).limit(1);

        if (result.length == 0) {
            return res.status(404).json({ error: `Transacoes com empresaId '${empresaId}' não encontradas ou vazias` });
        }
        return res.status(200).send(result);
    } catch (err) {
        console.log(err);
        next(err);
    }
});

// Listar as transacoes agrupadas por cartao da empresa logada
transacoesRouter.get('/empresa/cartoes', auth, async (req, res, next) => {
    try {
        const empresa = req.empresa;

        const empresaId = empresa._id;
        const result = await Transacoes.find({ 'empresaId': empresaId })
        var groupedResult  = _.groupBy(result, f => { return f.finalCartao });
        delete groupedResult[null];

        if (result.length == 0) {
            return res.status(404).json({ error: `Transacoes com empresaId '${empresaId}' não encontradas ou vazias` });
        }
        return res.status(200).send(groupedResult);
    } catch (err) {
        console.log(err);
        next(err);
    }
});

// Listar as transacoes de um cartao
transacoesRouter.get('/empresa/cartoes/:digitos', auth, async (req, res, next) => {
    try {
        let digitos = req.params.digitos;
        const empresa = req.empresa;

        const empresaId = empresa._id;
        const result = await Transacoes.find({ 'empresaId': empresaId, 'finalCartao': String(digitos) })

        if (result.length == 0) {
            return res.status(404).json({ error: `Transacoes com empresaId '${empresaId}' e digito '${digitos}' não encontradas ou vazias` });
        }
        return res.status(200).send(result);
    } catch (err) {
        console.log(err);
        next(err);
    }
});

// Lista transacoes de uma empresa especificada pelo ID
// Filtragem feita pela data de transacao (opcional) e flag de credito
transacoesRouter.get('/:empresaId/filtro', async (req, res, next) => {
    try {
        let empresaId = req.params.empresaId;
        let initialDate = req.query.inicio || new Date(0);
        let finalDate = req.query.fim || new Date(Date.now());
        let isCredit = Number(req.query.credito);

        if (isCredit === undefined) {
            queryFilter = { 
                'empresaId': empresaId, 
                'dataTransacao': { "$gte": initialDate, "$lte": finalDate }
            };
        }
        else {
            queryFilter = { 
                'empresaId': empresaId, 
                'dataTransacao': { "$gte": initialDate, "$lte": finalDate },
                'credito': !!isCredit
            };
        }

        let transacoes = await Transacoes.find(queryFilter);
        if (transacoes === undefined || transacoes.length == 0) {
            return res.status(404).send(`Transacoes with empresaId:'${empresaId}' not found`)
        }

        return res.status(200).send(transacoes);
    } catch (err) {
        console.log(err);
        next(err);
    }
});

module.exports = transacoesRouter;