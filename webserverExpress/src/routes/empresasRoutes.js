const express = require("express");
const empresasRouter = express.Router();
const Empresas = require("../models/empresas");
const {auth} = require("../middlewares/auth");
 

// signup
empresasRouter.post('/signup', async function(req, res, next) {
    try {
        const newUser = new Empresas(req.body);
        
        Empresas.findOne({ email: newUser.email }, function(err, user) {
            if (user) {
                return res.status(200).json({ success: false, error: "Email já cadastrado" }); 
            }
    
            newUser.save((err, doc) => {
                if (err) {
                    console.log(err);
                    //next(err);
                    return res.status(404).json({ error: err });
                }
                res.status(200).json({
                    success: true,
                    user: doc
                });
            });
        });
    } catch (err) {
        console.log(err);
        next(err);       
    }
});

// signin
empresasRouter.post('/signin', async function(req, res, next) {
    let token = req.cookies.auth;
    Empresas.findByToken(token, (err, user) => {
        if (err) return next(err);
        if (user) {
            return res.status(200).json({ isAuth: false, msg: "Este usuário/empresa já está logada" });
        }
        else {
            Empresas.findOne({ 'email': req.body.email }, function(err, user) {
                if (!user) {
                    return res.status(200).json({ isAuth: false, msg: "Email não cadastrado" });
                }
        
                user.compareHash(req.body.senha, (err, isMatch) => {
                    if (!isMatch) {
                        return res.status(200).json({ isAuth: false, msg: "Senha incorreta" });
                    }
        
                    user.generateToken((err, user) => {
                        if (err) return next(err);
                        res.cookie('auth', user.token, { expires: new Date(Date.now() + 900000), httpOnly: true }).json({
                            isAuth : true,
                            id : user._id,
                            email : user.email
                        });
                    });    
                });
            });
        }
    });
});

// logout
empresasRouter.get('/signout', auth, async function(req, res) {
    let token = req.cookies.auth;
    const empresa = req.empresa;
    empresa.deleteToken(token, (err, empresa) => {
        if (err) return res.status(400).send(err);
        res.status(200).json({ success: true });
    });
});

// Listar os dados de uma empresa especificada pelo ID
empresasRouter.get('/:empresaId', async (req, res, next) => {
    try {
        let empresaId = req.params.empresaId;
        const empresa = await Empresas.find({ '_id': empresaId });
        if (empresa.length == 0) {
            return res.status(404).json({ error: `Empresa com _id '${empresaId}' não encontrada` });
        }

        return res.status(200).send(empresa);
    } catch (err) {
        console.log(err);
        next(err);
    }
});


// Lista o saldo da conta de uma empresa especificada pelo ID
empresasRouter.get('/:empresaId/saldo', async (req, res, next) => {
    try {
        let empresaId = req.params.empresaId;
        const empresa = await Empresas.find({ '_id': empresaId });
        if (empresa.length == 0) {
            return res.status(404).json({ error: `Empresa com _id '${empresaId}' não encontrada` });
        }

        return res.status(200).send({ "saldo": empresa[0].saldo });
    } catch (err) {
        console.log(err);
        next(err);
    }
});

module.exports = empresasRouter;