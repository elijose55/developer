const config = {
    prod: {
        SECRET: process.env.SECRET,
        MONGO_DB_URI: process.env.MONGODB_URI
    },
    default: {
        SECRET: 'contasimples',
        MONGO_DB_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/contasimples'


    }
}

exports.get = function get(env) {
    return config[env] || config.default
}