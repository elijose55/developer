const express = require('express');
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require("mongoose");
const config = require('./config/config').get(process.env.NODE_ENV);


const server = express();
const port = 8000;
server.use(bodyparser.urlencoded({ extended: false }));
server.use(bodyparser.json());
server.use(cookieParser());

const uri = config.MONGO_DB_URI;
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }).then(
    () => {
        console.log("Database connection established!");
    },
    err => {
        console.log("Error connecting to Database instance due to: ", err);
    }
);
const db = mongoose.connection


const empresasRouter = require("./routes/empresasRoutes");
server.use("/empresas", empresasRouter);

const transacoesRouter = require("./routes/transacoesRoutes");
server.use("/transacoes", transacoesRouter);

async function healthcheckFunction(req, res) {
    res.status(200).send('Healthy');
}

server.get("/healthcheck", async (req, res) => healthcheckFunction(req, res));

server.listen(port, () => {
    console.log(`Server listening at ${port}`);
});