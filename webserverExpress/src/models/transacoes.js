const mongoose = require("mongoose");

const transacoesSchema = new mongoose.Schema({
    empresaId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Empresas"
    },
    dataTransacao: Date,
    finalCartao: String,
    tipoTransacao: String,
    descricaoTransacao: String,
    estabelecimento: String,
    credito: Boolean
});

module.exports = mongoose.model("Transacoes", transacoesSchema);