const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/config').get(process.env.NODE_ENV);

const salt = 7;

const empresasSchema = new mongoose.Schema({
    nomeEmpresa: {
        type: String,
        required: true,
        maxlength: 70,
        unique: 1
    },
    cnpj: {
        type: String,
        required: true,
        maxlength: 14,
        unique: 1
    },
    dadosBancario: {
        banco: {
            type: Number,
            required: true
        },
        bancoNome: {
            type: String,
            required: true,
            maxlength: 50
        },
        agencia: {
            type: Number,
            required: true
        },
        conta: {
            type: Number,
            required: true
        },
        digitoConta: {
            type: String,
            required: true,
            maxlength: 1
        }
    },
    saldo: {
        type: Number,
        required: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    senha: {
        type: String,
        required: true,
        minlength: 8
    },
    token: {
        type: String
    }
});

// funcao para gerar e salvar o hash da senha
empresasSchema.pre('save', function(next) {
    var empresa = this;
    
    if (empresa.isModified('senha')) {
        bcrypt.genSalt(salt, function(err, salt) {
            if (err) { return next(err) };

            bcrypt.hash(empresa.senha, salt, function(err, hash) {
                if (err) { return next(err) };
                empresa.senha = hash;
                next();
            })

        })
    }
    else {
        next();
    }
});

// funcao para comparar o hash senha inserida com o hash salvo
empresasSchema.methods.compareHash = function(senha, cb) {
    bcrypt.compare(senha, this.senha, function(err, isMatch) {
        if (err) { 
            console.log(err);
            return cb(err) 
        };
        cb(null, isMatch);
    });
}

// funcao para gerar um token de login
empresasSchema.methods.generateToken = function(cb) {
    var empresa = this;
    var token = jwt.sign(empresa._id.toHexString(), config.SECRET);

    empresa.token = token;
    empresa.save(function(err, empresa) {
        if (err) { return cb(err) };
        cb(null, empresa);
    })
}

// funcao para encontrar uma empresa/usuario pelo token
empresasSchema.statics.findByToken = function(token,cb){
    var empresa = this;

    jwt.verify(token, config.SECRET, function(err, decode) {
        empresa.findOne({ "_id": decode, "token": token }, function(err, empresa) {
            if (err) { return cb(err) };
            cb(null, empresa);
        })
    })
};

// funcao para deletar um token (sign out)
empresasSchema.methods.deleteToken = function(token, cb) {
    var empresa = this;
    empresa.updateOne({$unset: { token: 1 }}, function(err, empresa) {
        if (err) { return cb(err) };
        cb(null, empresa);
    })
}

module.exports = mongoose.model("Empresas", empresasSchema);