const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB({ region: 'us-east-1', apiVersion: '2012-08-10' });

exports.handler =  (event, context, cb) => {
   const params = {
       TableName: 'empresas'
   };
   const empresaId = event['pathParameters']['empresaId'];
   
   dynamoDB.scan(params, (err, data) => {
       if (err) {
           console.log(err);
           cb(err);
       } else {
            const unmarshalledData = data.Items.map(i => {
                return AWS.DynamoDB.Converter.unmarshall(i);
            });
            var filteredData = unmarshalledData.filter(function (el) {
                return el._id == empresaId;
            });


            const response = {
                "statusCode": 200,
                "body": JSON.stringify(filteredData),
                "headers": {
                    "Access-Control-Allow-Origin": "*",
	                "Access-Control-Allow-Credentials": true
                }
            };
           console.log(response);
           cb(null, response);
       }
   });
};
