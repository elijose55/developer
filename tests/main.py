import requests
import pytest
import json
from pymongo import MongoClient
from bson import ObjectId
from dateutil import parser


API_URL = "http://localhost:8000"
mongo_client = MongoClient("mongodb://localhost:27017/")
db = mongo_client["contasimples"]

def setup_module(module):
    empresas_collection = db["empresas"]
    transacoes_collection = db["transacoes"]

    with open('mocks/empresas.json', encoding='utf-8') as f:
        empresas_data = json.load(f)

    with open('mocks/transacoes.json', encoding='utf-8') as t:
        transacoes_data = json.load(t)

    for empresa in empresas_data:
        oid = ObjectId(empresa["_id"])
        empresa["_id"] = oid

    for transacao in transacoes_data:
        oid = ObjectId(transacao["empresaId"])
        transacao["empresaId"] = oid

        timestamp = parser.parse(transacao["dataTransacao"])
        transacao["dataTransacao"] = timestamp


    if isinstance(empresas_data, list):
        empresas_collection.insert_many(empresas_data)
    else:
        empresas_collection.insert_one(empresas_data)


    if isinstance(transacoes_data, list):
        transacoes_collection.insert_many(transacoes_data)
    else:
        transacoes_collection.insert_one(transacoes_data)

    print("Inserção realizada com sucesso!")
    mongo_client.close()

def teardown_module(module):
    db["transacoes"].delete_many({'empresaId': ObjectId("1111111fb2a8a9f1fe4ce6ec")})
    db["transacoes"].delete_many({'empresaId': ObjectId("2222222fb2a8a9f1fe4ce6ec")})
    db["empresas"].delete_one({'nomeEmpresa': "TEST1"})
    db["empresas"].delete_one({'nomeEmpresa': "TEST2"})
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})



def test_server_healthcheck_equals_200():
    response = requests.get(API_URL + "/healthcheck")
    assert response.status_code == 200


def test_get_transacoes_empresa_content_type_equals_json():
    response = requests.get(API_URL + "/transacoes/1111111fb2a8a9f1fe4ce6ec")
    assert response.headers["Content-Type"] == "application/json; charset=utf-8"


def test_get_transacoes_empresa_item_has_all_keys():
    response = requests.get(API_URL + "/transacoes/1111111fb2a8a9f1fe4ce6ec")
    check = True

    keys = ["_id", "empresaId", "dataTransacao", "valor",
            "finalCartao", "tipoTransacao", "descricaoTransacao",
            "estabelecimento", "credito"]
    
    for k in keys:
        if k not in response.json()[0]:
            check = False
            break

    assert check

def test_get_transacoes_empresa_returns_correct_len():
    response = requests.get(API_URL + "/transacoes/1111111fb2a8a9f1fe4ce6ec")
    assert (len(response.json()) != 0) and ("error" not in response.json())

def test_get_transacoes_empresa_returns_correct_transactions():
    empresaId = "1111111fb2a8a9f1fe4ce6ec"
    response = requests.get(API_URL + "/transacoes/" + empresaId)
    check = True

    for transaction in response.json():
        if transaction["empresaId"] != empresaId:
            check = False
            break

    assert check

def test_get_transacoes_empresa_item_has_all_keys():
    response = requests.get(API_URL + "/transacoes/1111111fb2a8a9f1fe4ce6ec")
    check = True

    keys = ["_id", "empresaId", "dataTransacao", "valor",
            "finalCartao", "tipoTransacao", "descricaoTransacao",
            "estabelecimento", "credito"]
    
    for k in keys:
        if k not in response.json()[0]:
            check = False
            break

    assert check

def test_get_last_transacao_empresa_returns_one():
    r = requests.Session()
    sign_in_json = {
	    "email": "test1@hotmail.com",
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    response_success = r.post(url, json=sign_in_json)

    response = r.get(API_URL + "/transacoes/empresa/ultima")
    assert (len(response.json()) == 1) and ("error" not in response.json())

def test_get_last_transacao_empresa_returns_last_transaction():
    response_all = requests.get(API_URL + "/transacoes/1111111fb2a8a9f1fe4ce6ec")

    sign_in_json = {
	    "email": "test1@hotmail.com",
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    r = requests.Session()
    response_success = r.post(url, json=sign_in_json)
    response_last = r.get(API_URL + "/transacoes/empresa/ultima")

    last_date = response_all.json()[0]["dataTransacao"]
    for t in response_all.json():
        if t["dataTransacao"] > last_date:
            last_date = t["dataTransacao"]

    assert response_last.json()[0]["dataTransacao"] == last_date

def test_get_transacao_empresa_cartao_returns_correct_card_keys():
    sign_in_json = {
	    "email": "test1@hotmail.com",
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    r = requests.Session()
    response_success = r.post(url, json=sign_in_json)

    response = r.get(API_URL + "/transacoes/empresa/cartoes")
    print(response)
    assert "1111" in response.json() and "2222" in response.json()

def test_get_transacao_empresa_cartao_returns_correct_transacao():
    sign_in_json = {
	    "email": "test1@hotmail.com",
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    r = requests.Session()
    response_success = r.post(url, json=sign_in_json)
    response = r.get(API_URL + "/transacoes/empresa/cartoes/1111")
    check = True

    for t in response.json():
        if t["finalCartao"] != "1111":
            check = False
            break

    assert check

def test_get_transacao_empresa_filter_returns_correct_dates_and_flag():
    url = ("/transacoes/1111111fb2a8a9f1fe4ce6ec/filtro?"
            "inicio=09%2F03%2F2020&fim=09%2F08%2F2020&credito=0")

    response = requests.get(API_URL + url)
    check = True

    for t in response.json():
        initial_timestamp = parser.parse("2020-09-03T20:50:00.000Z")
        final_timestamp = parser.parse("2020-09-08T20:50:00.000Z")
        response_timestamp = parser.parse(t["dataTransacao"])

        if response_timestamp < initial_timestamp:
            check = False
            break
        elif response_timestamp > final_timestamp:
            check = False
            break
        elif t["credito"] == True:
            check = False
            break

    assert check

def test_get_empresa_content_type_equals_json():
    response = requests.get(API_URL + "/empresas/1111111fb2a8a9f1fe4ce6ec")
    assert response.headers["Content-Type"] == "application/json; charset=utf-8"

def test_get_empresa_item_has_all_keys():
    response = requests.get(API_URL + "/empresas/1111111fb2a8a9f1fe4ce6ec")
    check = True

    keys = ["_id", "nomeEmpresa", "cnpj", "dadosBancario",
            "saldo", "email"]
    
    for k in keys:
        if k not in response.json()[0]:
            check = False
            break

    assert check

def test_get_empresa_returns_correct_saldo():
    response = requests.get(API_URL + "/empresas/1111111fb2a8a9f1fe4ce6ec/saldo")
    assert response.json()["saldo"] == 10000

def test_empresa_signup_returns_success():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": "signup@hotmail.com",
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    response = requests.post(url, json=sign_up_json)
    assert response.json()["success"]

def test_empresa_signup_repeat_returns_error():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": "signup@hotmail.com",
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    response_success = requests.post(url, json=sign_up_json)
    response_error = requests.post(url, json=sign_up_json)
    assert response_error.json()["success"] == False

def test_empresa_signin_correct_password_returns_success():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})
    email = "signup@hotmail.com"
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": email,
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    response = requests.post(url, json=sign_up_json)

    sign_in_json = {
	    "email": email,
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    response = requests.post(url, json=sign_in_json)

    assert response.json()["isAuth"] and response.json()["email"] == email

def test_empresa_signin_wrong_password_returns_error():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})
    email = "signup@hotmail.com"
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": email,
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    response = requests.post(url, json=sign_up_json)

    sign_in_json = {
	    "email": email,
	    "senha": "123456789"
    }

    url = API_URL + "/empresas/signin"
    response = requests.post(url, json=sign_in_json)

    assert response.json()["isAuth"] == False

def test_empresa_signin_already_signed_returns_error():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})

    email = "signup@hotmail.com"
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": email,
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    r = requests.Session() 
    response = r.post(url, json=sign_up_json)

    sign_in_json = {
	    "email": email,
	    "senha": "1234567890"
    }

    url = API_URL + "/empresas/signin"
    response_success = r.post(url, json=sign_in_json)
    response_error = r.post(url, json=sign_in_json)
    print(response_error.json())

    assert (response_error.json()["isAuth"] == False and
           response_error.json()["msg"] == "Este usuário/empresa já está logada")

def test_empresa_signout_returns_success():
    db["empresas"].delete_one({'nomeEmpresa': "TEST3"})

    email = "signup@hotmail.com"
    sign_up_json = 	{
		"nomeEmpresa": "TEST3",
		"cnpj": "11111111111172",
		"dadosBancario": {
			"banco": 999,
			"bancoNome": "CONTA SIMPLES",
			"agencia": 1,
			"conta": 123456,
			"digitoConta": "1"
		},
		"saldo": 10500.00,
		"email": email,
		"senha": "1234567890"
	}

    url = API_URL + "/empresas/signup"
    r = requests.Session() 
    response = r.post(url, json=sign_up_json)

    sign_in_json = {
	    "email": email,
	    "senha": "1234567890"
    }

    signin_url = API_URL + "/empresas/signin"
    signout_url = API_URL + "/empresas/signout"

    response = r.post(signin_url, json=sign_in_json)
    response_signout = r.get(signout_url)
    print(response_signout.json())

    assert response_signout.json()["success"] == True