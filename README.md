# Teste Conta Simples

### Autor: Eli Jose Abi Ghosn

<!-- blank line -->
----
<!-- blank line -->

Olá, primeiramente, obrigado pela oportunidade!

Neste repositório está contida a resolução para o desafio do processo seletivo da Conta Simples. Escolhi realizar apenas o desafio do backend, pois é nesta área que tenho mais expertise e gostaria de trabalhar. O enunciado completo do desafio está no arquivo `Desafio.md`. Abaixo estão as tarefas realizadas:

1. Criar uma API de login;
2. Criar uma API que retornar o saldo da conta; 
3. Cria API de extrato da conta -> filtrar por Data de Transação e flag de "Crédito" e "Débito";
4. Criar API pra retornar o última transação realizada da empresa logada;
5. Endpoint para retornar transações agrupadas por cartão;

 
# Backend ExpressJS

Inicialmente, desenvolvi um webserver em Node com o uso do framework Express. Todas as tarefas pedidas foram completadas nesta parte do projeto.

#### Linguagem: JS
#### Bibliotecas/Frameworks utilizados:
- Node.js
- Express.js
- Mongoose

#### Diretório: */webserverExpress*
#### Porta de Execução: 8000
#### Dockerfile: */webserverExpress/Dockerfile*

### Organização:
O diretório **src** contém os executáveis do webserver. O executável principal é o **app.js**, nele são configurados a conexão com a base de dados e algumas extensões e configurações gerais do server. Além disso, são importados os executáveis que contém as rotas do webserver. 

O subdiretório **routes** contém as rotas utilizadas pelo server, o **config** armazena o arquivo de configuração, o **middlewares** guarda as funções intermediárias usadas pelas rotas e o **models** contém os Schemes de cada coleção da base de dados do Mongo (Empresas e Transações).


### Execução Local:

Para testar localmente o webserver do backend é preciso inicialmente possuir um servidor MongoDB, localmente ou remotamente. foi desenvolvido um script em Python para popular automáticamente uma base de dados do Mongo com os dados mock fornecidos. Para executar o script, você precisa ter uo Python 3+ instalado em sua máquina. Então, no diretório **/mongoScript**, rode o seguinte comando para instalar as dependências:

```bash
pip install -r requirements.txt
```

Depois é preciso configurar a variável de ambiente MONGODB_URI com a URI de conexão ao MongoDB, caso queria se conectar com algum servidor MongoDB sem ser o local ('mongodb://localhost:27017/contasimples'). No Ubuntu isso pode ser feito com o seguinte comando:

```bash
export MONGODB_URI='mongodb://localhost:27017/contasimples'
```

Por fim, basta executar o próprio script de Python (qualquer SO):
```bash
python populate.py
```

Finalizada a etapa de configuração e preenchimento do banco de dados é preciso instalar o node.js e o npm em sua máquina, caso não os tenha. No Ubuntu isso pode ser feito com o seguinte comando:

```bash
sudo apt install nodejs
```

Em seguida, executar o seguinte comando em **/webserverExpress** para instalar as bibliotecas necessárias (qualquer SO):

```bash
npm install
```

E por fim para iniciar o server (qualquer SO):

```bash
npm start
```

Nenhum erro deve aparecer após a execução deste comando e para testar se o webserver está executando corretamente basta acessar a rota **localhost:8000/healthcheck**

### Execução na EC2:

Além da execução local, realizei o deploy da aplicação para uma máquina na EC2 da AWS. Para isso utilizei o Dockerfile disponível em **/webserverExpress** para subir o webserver em um conteiner e o Dockerfile oficial do Mongo (https://hub.docker.com/_/mongo) para subir um servidor do mesmo em outro conteiner. É importante notar que utilizei uma instância da EC2 para hostear o Mongo, ao invez de usar o DocumentDB da AWS, pois o segundo não está no Free Tier da AWS.

Todas as funcionalidades e rotas do webserver local estão disponíveis no webserver remoto, no entanto, os testes unitários apenas funcionam com o local, uma vez que seria necessário expôr o servidor Mongo da EC2 a Internet para que os testes funcionassem corretamente.

Para testar o webserver remoto basta acessar: http://54.144.225.31:8000/healthcheck


**Observação:** Para execução local do webserver, o servidor de Mongo não utiliza nenhuma autenticação, porém para o servidor presente na instância da EC2 foi configurada uma autenticação. Caso isso não fosse um exercício fictício, o Mongo de produção estaria como no segundo caso, com autenticação configurada.

Além disso, tenho pleno conhecimento de que nunca se deve expôr quaisquiser tipos de chaves, tokens, senhas ou hashes em repositórios públicos ou que poderão se tornar públicos no futuro. O arquivo config.js possui um secret exposto, que em um projeto real seria substituido por uma variável de ambiente.


# Testes unitários

Os testes unitários para o webserver foram desenvolvidos em Python com a biblioteca Pytest e estão no diretório **/tests** Para executar os testes é preciso primeiramente executar com sucesso o webserver local. Então:
- Instalar ou ter instalado algum Python 3+
- Instalar o pytest para a mesma versao existente do Python
- Instalar as dependências com o comando:
    ```bash
    pip install -r requirements.txt
    ```
- Por fim, executar os testes:
    ```bash
    pytest main.py
    ```

# Serverless API

Além do webserver em ExpressJS, também foi desenvolvido uma API utilizando Lambda + API Gateway + DynamoDB. Isso foi feito para demonstrar minha capacidade de aprendizado e dedicação frente novos desafios e tecnologias. No entanto, apenas duas rotas foram criadas nessa stack, já que o restante seria apenas um trabalho quase manual de adaptação do modelo criado em ExpressJS. As duas rotas criadas foram:

- https://go6wo7t0m5.execute-api.us-east-1.amazonaws.com/dev/empresas/5f86600fb2a8a9f1fe4ce6ec - Retorna os dados de uma empresa determinada pelo seu id (5f86600fb2a8a9f1fe4ce6ec).



- https://go6wo7t0m5.execute-api.us-east-1.amazonaws.com/dev/empresas/5f86600fb2a8a9f1fe4ce6ec/saldo - Retorna o saldo de uma empresa determinada pelo seu id (5f86600fb2a8a9f1fe4ce6ec).

As funções do Lambda criadas estão no diretório **/lambda**.

# Documentação das tarefas/rotas

O correto seria gerar uma documentação para cada rota criada com o formato de request, response, etc em especificações como OpenAPI. Porém, dei prioridade para outras partes do desafio dentro do tempo fornecido. Então, abaixo estão descritas de maneira geral as rotas criadas para cada tarefa.

1. Criar uma API de login;

    **Sign-up:**
    ```bash
    curl --request POST \
    --url 'http://localhost:8000/empresas/signup' \
    --header 'content-type: application/json' \
    --data '	{
            "nomeEmpresa": "TESTE_DESAFIO",
            "cnpj": "11111111111172",
            "dadosBancario": {
                "banco": 999,
                "bancoNome": "CONTA SIMPLES",
                "agencia": 1,
                "conta": 123456,
                "digitoConta": "1"
            },
            "saldo": 10500.00,
            "email": "desafio@hotmail.com",
            "senha": "1234567890"
        }'
    ```

    **Sign-in:**
    ```bash
    curl --request POST \
    --url 'http://localhost:8000/empresas/signin' \
    --header 'content-type: application/json' \
    --data '{
        "email": "desafio@hotmail.com",
        "senha": "1234567890"
    }'
    ```

    **Sign-out:**
    ```bash
    curl --request GET \
    --url 'http://localhost:8000/users/signout' \
    --header 'content-type: application/json' \
    --cookie auth=eyJhbGciOiJIUzI1NiJ9.NWY4ZjY3NzRkMDYxMDYxY2UwYzY3OWZi.RYxP7dCnW_tspmg0s72PmUVeDM70y7lDGRTAVRT-3Eo
    ```

    Sendo o Cookie "**auth**" o token retornado quando o usuário realiza o sign-in.

2. Criar uma API que retornar o saldo da conta;

    ```bash
    curl --request GET \
    --url 'http://localhost:8000/empresas/5f86600fb2a8a9f1fe4ce6ec/saldo'
    ```

    Sendo "5f86600fb2a8a9f1fe4ce6ec" o id da empresa desejada.


3. Cria API de extrato da conta -> filtrar por Data de Transação e flag de "Crédito" e "Débito";

    ```bash
    curl --request GET \
    --url 'http://localhost:8000/transacoes/5f86600fb2a8a9f1fe4ce6ec/filtro?inicio=09%2F01%2F2020&fim=09%2F08%2F2020&credito=0'
    ```

    Sendo "5f86600fb2a8a9f1fe4ce6ec" o id da empresa desejada, e os parâmetros (todos opcionais):
    - inicio - Data inicial de filtragem 
    - fim - Data final de filtragem
    - credito - Flag de crédito (1-crédito/0-débito)

4. Criar API pra retornar o última transação realizada da empresa logada;

    ```bash
    curl --request GET \
    --url 'http://localhost:8000/transacoes/empresa/ultima' \
    --cookie auth=eyJhbGciOiJIUzI1NiJ9.NWY4NjYwMGZiMmE4YTlmMWZlNGNlNmVj.mzWSkuZShO5exYhbOSawrra7MoZTzgHuMHRcREIPJZk
    ```

    Sendo o Cookie "**auth**" o token retornado quando o usuário realiza o sign-in.

5. Endpoint para retornar transações agrupadas por cartão;

    ```bash
    curl --request GET \
    --url 'http://localhost:8000/transacoes/empresa/cartoes' \
    --cookie auth=eyJhbGciOiJIUzI1NiJ9.NWY4NjYwMGZiMmE4YTlmMWZlNGNlNmVj.mzWSkuZShO5exYhbOSawrra7MoZTzgHuMHRcREIPJZk
    ```

    Sendo o Cookie "**auth**" o token retornado quando o usuário realiza o sign-in.


# Próximos passos

- Documentação da API em OpenAPI com alguma ferramenta como Swagger.
- Cloudformation ou SAM para deploy automático da infraestrutura Lambda + API Gateway + DynamoDB.
- Docstrings das funções
- Documentar os testes unitarios.
- Melhorar e tornar API de login mais robusta e completa.
- Verficação de vulnerabilidades.
- CRUD completo para transações.

# Observações finais

Gostaria de destacar que caso este projeto fosse desenvolvido em condições reais, toda a metodologia de desenvolvimento seria feita por branches e PRs, com uma branch principal e outras criadas por cada integrante do time a cada sprint realizada. Assim, ao final da sprint seria realizado o merge das branches criadas na master. Concluindo, tenho total conhecimento sobre desenvolvimento colaborativo e metodologias agéis, tendo já contribuido para repositórios open source por meio dessas práticas.

Obrigado pela oportunidade de participar do processo seletivo da Conta Simples! 



